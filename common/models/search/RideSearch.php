<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ride;
use yii\db\Expression;

/**
 * RideSearch represents the model behind the search form of `common\models\Ride`.
 */
class RideSearch extends Ride
{
    public $assignedTo;
    public $exceptUserId;
    public $onlyFuture = false;
    public $district = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'user_id', 'sex', 'seats', 'created_at', 'updated_at', 'assignedTo', 'exceptUserId', 'district'], 'integer'],
            [['description', 'date_start', 'districts', 'rrules'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ride::find();

        // add conditions that should always apply here
        if (!empty($this->assignedTo)) {
            $query->joinWith('userRideAssignments')
                ->andWhere(['`user_ride_assignment`.user_id' => $this->assignedTo]);
        }
        $query->andFilterWhere(['!=', 'user_id', $this->exceptUserId]);
        if ($this->onlyFuture === true) {
            $query->andWhere(['>=', 'date_start', new Expression('NOW()')]);
        }
        if (!empty($this->district)) {
            $query->andWhere('FIND_IN_SET(' . $this->district . ', `districts`)');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date_start' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'user_id' => $this->user_id,
            'date_start' => $this->date_start,
            'sex' => $this->sex,
            'seats' => $this->seats,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'districts', $this->districts])
            ->andFilterWhere(['like', 'rrules', $this->rrules]);

        return $dataProvider;
    }
}
