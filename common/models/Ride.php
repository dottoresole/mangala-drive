<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ride".
 *
 * @property int $id
 * @property int $event_id
 * @property int $user_id
 * @property string $description
 * @property string $date_start
 * @property int $sex
 * @property string $districts
 * @property int $seats
 * @property string $rrules
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Event $event
 * @property User $user
 */
class Ride extends \yii\db\ActiveRecord
{
    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ride';
    }

    public function beforeValidate()
    {
        if (!empty($this->date_start)) {
            $this->date_start = date('Y-m-d', strtotime($this->date_start));
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'date_start', 'seats'], 'required'],
            [['event_id', 'user_id', 'sex', 'seats', 'created_at', 'updated_at', 'freeSeats'], 'integer'],
            [['description', 'districts', 'rrules'], 'string'],
            ['date_start', 'date', 'format' => 'php:Y-m-d'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
            ['districtsArray', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Событие',
            'user_id' => 'User ID',
            'description' => 'Описание',
            'date_start' => 'Дата поездки',
            'sex' => 'Пол',
            'districts' => 'Районы',
            'districtsArray' => 'Районы',
            'seats' => 'Всего мест',
            'rrules' => 'Rrules',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'freeSeats' => 'Свободные места',
        ];
    }

    public function getFreeSeats()
    {
        $free_seats = $this->seats - count($this->userRideAssignments);

        return $free_seats > 0 ? $free_seats : 0;
    }

    function getDistrictsArray()
    {
        return explode(',', $this->districts);
    }

    function setDistrictsArray(array $value)
    {
        $this->districts = implode(',', $value);
    }

    public static function getSexes()
    {
        return [
            self::SEX_MALE => 'Мужской',
            self::SEX_FEMALE => 'Женский',
        ];
    }

    /**
     * @return boolean
     */
    public function getIsFuture()
    {
        return strtotime($this->date_start) > time();
    }

    /**
     * @return boolean
     */
    public function getCanViewPassengers()
    {
        if ($this->user_id == Yii::$app->user->id) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function getCanCheckIn()
    {
        if ($this->isFuture && $this->freeSeats > 0 && $this->user_id != Yii::$app->user->id && !$this->getIsCheckedIn()) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function getIsCheckedIn($user_id = null)
    {
        if (empty($user_id)) {
            $user_id = Yii::$app->user->id;
        }

        if (!UserRideAssignment::findUserRide($this->id, $user_id)) {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function getCanRecord()
    {
        if ($this->isFuture && $this->freeSeats > 0 && $this->user_id == Yii::$app->user->id) {
            return true;
        }

        return false;
    }

    /**
     * @return Ride
     */
    public static function createClone($model)
    {
        $clone = new static;
        $clone->attributes = $model->attributes;
        $clone->date_start = null;

        return $clone;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRideAssignments()
    {
        return $this->hasMany(UserRideAssignment::className(), ['ride_id' => 'id']);
    }
}
