<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_ride_assignment".
 *
 * @property int $id
 * @property int $user_id
 * @property int $ride_id
 * @property int $seats_reserved
 * @property int $created_at
 *
 * @property Ride $ride
 * @property User $user
 */
class UserRideAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_ride_assignment';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ride_id', 'seats_reserved'], 'integer'],
            [['name', 'phone'], 'string'],
            [['ride_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ride::className(), 'targetAttribute' => ['ride_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'user_id' => 'User ID',
            'ride_id' => 'Ride ID',
            'seats_reserved' => 'Зарезервировано мест',
            'created_at' => 'Created At',
            'riderName' => 'Имя',
            'riderPhone' => 'Телефон',
            'riderEmail' => 'E-mail',
        ];
    }

    /**
     * @return string
     */
    public function getRiderName()
    {
        return !empty($this->user_id) ? $this->user->name : $this->name;
    }

    /**
     * @return string
     */
    public function getRiderPhone()
    {
        return !empty($this->user_id) ? $this->user->phone : $this->phone;
    }

    /**
     * @return string
     */
    public function getRiderEmail()
    {
        return !empty($this->user_id) ? $this->user->email : null;
    }

    public static function findUserRide($ride_id, $user_id)
    {
        return static::find()
            ->where([
                'ride_id' => $ride_id,
                'user_id' => $user_id,
            ])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRide()
    {
        return $this->hasOne(Ride::className(), ['id' => 'ride_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
