<?php

namespace common\models\traits;

use Yii;

trait UploadTrait
{
    public function uploadImageFile()
    {
        if (!empty($this->imageFile)) {
            $fileName = Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs(Yii::$app->params['file.uploadPath'] . $fileName)) {
                return $fileName;
            }
        }

        return false;
    }
}
