<?php

namespace common\models\traits;

use Yii;

trait CommonTrait
{
    public static function getAllRecords($param = null, $value = null)
    {
        $query = static::find();

        if (!empty($param) && !empty($value)) {
            $query->andWhere([$param => $value]);
        }

        return $query->all();
    }
}
