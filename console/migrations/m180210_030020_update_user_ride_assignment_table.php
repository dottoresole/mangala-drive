<?php

use yii\db\Migration;

/**
 * Class m180210_030020_update_user_ride_assignment_table
 */
class m180210_030020_update_user_ride_assignment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_ride_assignment', 'phone', $this->string()->after('ride_id'));
        $this->addColumn('user_ride_assignment', 'name', $this->string()->after('ride_id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_ride_assignment', 'phone');
        $this->dropColumn('user_ride_assignment', 'name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180210_030020_update_user_ride_assignment_table cannot be reverted.\n";

        return false;
    }
    */
}
