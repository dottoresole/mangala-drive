<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_ride_assignment`.
 */
class m180130_141453_create_user_ride_assignment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('user_ride_assignment', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'ride_id' => $this->integer(),
            'seats_reserved' => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-user_ride_assignment-user_id',
            'user_ride_assignment',
            'user_id',
            'user',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-user_ride_assignment-ride_id',
            'user_ride_assignment',
            'ride_id',
            'ride',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_ride_assignment');
    }
}
