<?php

use yii\db\Migration;

/**
 * Handles the creation of table `district`.
 */
class m171219_084306_create_district_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('district', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'city_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-district-city',
            'district',
            'city_id',
            'city',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-event-city',
            'event',
            'district_id',
            'district',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('district');
    }
}
