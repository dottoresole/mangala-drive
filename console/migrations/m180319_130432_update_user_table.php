<?php

use yii\db\Migration;

/**
 * Class m180319_130432_update_user_table
 */
class m180319_130432_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'district_id', $this->integer()->after('email'));
        $this->addColumn('user', 'city_id', $this->integer()->after('email'));
        $this->addColumn('user', 'country_id', $this->integer()->after('email'));
        $this->addColumn('user', 'image', $this->string());

        $this->addForeignKey(
            'fk-user-country_id',
            'user',
            'country_id',
            'country',
            'id',
            'SET NULL',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-user-city_id',
            'user',
            'city_id',
            'city',
            'id',
            'SET NULL',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-user-district_id',
            'user',
            'district_id',
            'district',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user-country_id', 'user');
        $this->dropForeignKey('fk-user-city_id', 'user');
        $this->dropForeignKey('fk-user-district_id', 'user');
        $this->dropColumn('user', 'district_id');
        $this->dropColumn('user', 'city_id');
        $this->dropColumn('user', 'country_id');
        $this->dropColumn('user', 'image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_130432_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
