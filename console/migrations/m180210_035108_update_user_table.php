<?php

use yii\db\Migration;

/**
 * Class m180210_035108_update_user_table
 */
class m180210_035108_update_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'sex', $this->smallInteger()->after('email'));
        $this->addColumn('user', 'phone', $this->string()->after('email'));
        $this->addColumn('user', 'name', $this->string()->after('email'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'sex');
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180210_035108_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
