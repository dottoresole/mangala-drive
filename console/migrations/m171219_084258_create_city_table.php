<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m171219_084258_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-city-country',
            'city',
            'country_id',
            'country',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
    }
}
