<?php

use yii\db\Migration;

/**
 * Handles the creation of table `event`.
 */
class m171219_084243_create_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'district_id' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('event');
    }
}
