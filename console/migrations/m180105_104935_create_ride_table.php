<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ride`.
 */
class m180105_104935_create_ride_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('ride', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer(),
            'user_id' => $this->integer(),
            'description' => $this->text(),
            'date_start' => $this->date(),
            'sex' => $this->smallInteger(),
            'districts' => $this->text(),
            'seats' => $this->integer(),
            'rrules' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-ride-user_id',
            'ride',
            'user_id',
            'user',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-ride-event_id',
            'ride',
            'event_id',
            'event',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ride');
    }
}
