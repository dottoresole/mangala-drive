<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends Controller
{
	public function actionInit()
	{
		$auth = Yii::$app->authManager;

        // Суперадминистратор
		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

    // Присваивает роль суперадмина пользователю с ID $id
	public function actionAddAdmin($id)
	{
		$auth = Yii::$app->authManager;

		$admin = $auth->getRole('admin');
		$auth->assign($admin, $id);
	}
}
