<?php

namespace frontend\controllers;

use Yii;
use yii\web\Response;
use common\models\Country;
use common\models\City;
use common\models\District;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class DataController extends Controller
{
    public function actionCity()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                $out = City::getAllRecords('country_id', $country_id);

                return ['output' => $out, 'selected' => ''];
            }
        }

        return ['output' => '', 'selected' => ''];
    }

    public function actionDistrict()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                $city_id = $parents[1];
                if (!empty($country_id) && !empty($city_id)) {
                    $out = District::getAllRecords('city_id', $city_id);
                    if (!empty($out)) {
                        return ['output' => $out, 'selected' => ''];
                    }
                }
            }
        }

        return ['output' => '', 'selected' => ''];
    }
}
