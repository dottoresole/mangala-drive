<?php

namespace frontend\controllers;

use common\models\search\UserRideAssignmentSearch;
use frontend\models\CheckInForm;
use frontend\models\Record;
use Yii;
use common\models\Ride;
use common\models\search\RideSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RideController implements the CRUD actions for Ride model.
 */
class RideController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ride models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RideSearch(['user_id' => Yii::$app->user->id]);
        $dataProvider = $searchModel->search(null);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMyRides()
    {
        $searchModel = new RideSearch(['assignedTo' => Yii::$app->user->id]);
        $dataProvider = $searchModel->search(null);

        return $this->render('my-rides', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new RideSearch([
            'exceptUserId' => Yii::$app->user->id,
            'district' => Yii::$app->user->identity->district_id,
            'onlyFuture' => true,
        ]);
        $dataProvider = $searchModel->search(null);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClone($id)
    {
        $model = $this->findModel($id);
        /** @var $clone Ride */
        $clone = Ride::createClone($model);

        if ($clone->load(Yii::$app->request->post()) && $clone->save()) {
            return $this->redirect(['view', 'id' => $clone->id]);
        }

        return $this->render('create', [
            'model' => $clone,
        ]);
    }

    public function actionCheckIn($id)
    {
        $ride = $this->findModel($id);

        if (!$ride->getCanCheckIn()) {
            throw new ForbiddenHttpException('Действие запрещено.');
        }

        /** @var $model CheckInForm */
        $model = new CheckInForm(['model' => $ride]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Вы успешно записались на поездку.');

            return $this->redirect(['my-rides']);
        }

        return $this->render('check-in', [
            'model' => $model,
        ]);
    }

    public function actionRecord($id)
    {
        $ride = $this->findModel($id);
        /** @var $model Record */
        $model = new Record(['model' => $ride]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Вы успешно добавили пассажира в свою поездку.');

            return $this->redirect(['view', 'id' => $ride->id]);
        }

        return $this->render('record', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Ride model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new UserRideAssignmentSearch(['ride_id' => $model->id]);
        $dataProvider = $searchModel->search(null);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Ride model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ride();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ride model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ride model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ride model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ride the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ride::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
