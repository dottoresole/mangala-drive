<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
?>
<div class="site-login">
    <p>Пожалуйста, заполните эти поля, чтобы войти в систему:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                Вы можете <?= Html::a('зарегистрировать нового пользователя', ['site/signup']) ?> или <?= Html::a('восстановить пароль', ['site/request-password-reset']) ?>.
            </div>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-default btn-orange', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
