<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ProfileForm */
/* @var $modelPassword \frontend\models\ChangePasswordForm */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-success">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>

                <?= $form->field($model, 'name') ?>

                <?= $form->field($model, 'phone') ?>

                <p><b>E-mail:</b> <?= $model->email ?></p>
                <p><b>Пол:</b> <?= User::getSexes()[$model->sex] ?></p>
                <p><b>Регион:</b> <?= $model->region ?></p>

                <?php if (!empty($model->image)): ?>
                    <div class="form-group">
                        <label class="control-label">Ваша фотография</label>
                        <div><img width="150" src="/uploads/photos/<?= $model->image ?>"/></div>
                    </div>
                <?php endif; ?>

                <div class="form-group pull-left">
                    <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary', 'name' => 'profile-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-success">
            <div class="panel-heading">Изменить пароль</div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => 'form-profile-2']); ?>

                <?= $form->field($modelPassword, 'passwordOld')->passwordInput() ?>

                <?= $form->field($modelPassword, 'password')->passwordInput() ?>

                <?= $form->field($modelPassword, 'passwordRepetition')->passwordInput() ?>

                <div class="form-group pull-left">
                    <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
