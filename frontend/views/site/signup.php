<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DepDrop;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <p>Пожалуйста, заполните эти поля, чтобы стать зарегистрированным пользователем:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'name') ?>

            <?= $form->field($model, 'phone') ?>

            <?= $form->field($model, 'sex')->radioList(\common\models\User::getSexes()) ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'country')->dropDownList(ArrayHelper::map(\common\models\Country::getAllRecords(), 'id', 'name'), ['id' => 'country-id', 'prompt' => 'Выберите..']); ?>

            <?= $form->field($model, 'city')->widget(DepDrop::class, [
                'options' => ['id' => 'city-id'],
                'pluginOptions' => [
                    'depends' => ['country-id'],
                    'placeholder' => 'Выберите..',
                    'url' => Url::to(['/data/city'])
                ]
            ]); ?>

            <?= $form->field($model, 'district')->widget(DepDrop::class, [
                'options' => ['id' => 'district-id'],
                'pluginOptions' => [
                    'depends' => ['country-id', 'city-id'],
                    'placeholder' => 'Выберите..',
                    'url' => Url::to(['/data/district'])
                ]
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'imageFile')->fileInput() ?>

            <?= $form->field($model, 'acceptRules')->checkbox(['label' => 'Регистрируясь, я принимаю правила системы, расположенные <a href="' . Url::to(['/site/page', 'view' => 'rules']) . '">по этому постоянному адресу</a>']) ?>

            <div class="form-group">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-default btn-orange', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
