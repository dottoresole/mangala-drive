<?php

/* @var $this yii\web\View */

$this->title = 'Mangala Drive';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
    <div class="jumbotron">
        <h2>Для пассажиров</h2>

        <p class="lead">Текст, раскрывающий преимущества для пассажиров.</p>

        <p><a class="btn btn-lg btn-warning" href="<?= \yii\helpers\Url::to(['/ride/my-rides']) ?>">Я &mdash; пассажир</a></p>
    </div>
            </div>
            <div class="col-lg-6">
    <div class="jumbotron">
        <h2>Для водителей</h2>

        <p class="lead">Текст, раскрывающий преимущества для водителей.</p>

        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['/ride/index']) ?>">Я &mdash; водитель</a></p>
    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <?= $this->render('/site/pages/rules') ?>
            </div>
        </div>

    </div>
</div>
