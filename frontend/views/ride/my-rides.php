<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои поездки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-index">
    <p>
        <?= Html::a('Найти машину', ['search'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Мои запросы на доставку', ['/request/index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => false,
        'rowOptions' => function ($model) {
            if ($model->isFuture) {
                return ['class' => 'success'];
            }

            return null;
        },
        'columns' => [
            'event.name',
            'date_start:date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{checkout} {view}',
                'buttons' => [
                    'checkout' => function ($url, $model, $key) {
                        return Html::a('Отказаться', ['check-out', 'id' => $model->id], ['title' => 'Отказаться', 'class' => 'btn btn-xs btn-default']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
