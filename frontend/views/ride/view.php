<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Ride */
/* @var $searchModel common\models\search\UserRideAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->event->name;
$this->params['breadcrumbs'][] = ['label' => $model->canViewPassengers ? 'Мои поездки' : 'Поиск поездок', 'url' => $model->canViewPassengers ? ['index'] : ['search']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-view">

    <h4>Дата старта: <?= Yii::$app->formatter->asDate($model->date_start) ?></h4>
    <p><?= nl2br($model->description) ?></p>

    <?php if ($model->canViewPassengers): ?>
    <h3>Записанные пассажиры</h3>
    <?php if ($model->canRecord): ?>
    <p>
        <?= Html::a('Записать на поездку', ['record', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'riderName',
            'riderPhone',
            'riderEmail:email',
            'seats_reserved',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{remove-user}',
                'buttons' => [
                    'remove-user' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => 'Удалить пассажира']);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php elseif ($model->canCheckIn): ?>
        <p>
            <?= Html::a('Записаться на поездку', ['check-in', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
</div>
