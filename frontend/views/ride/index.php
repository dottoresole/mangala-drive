<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои поездки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-index">

    <p>
        <?= Html::a('Создать поездку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => false,
        'rowOptions' => function ($model) {
            if ($model->isFuture) {
                return ['class' => 'success'];
            }

            return null;
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'event.name',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model common\models\Ride */
                    return Html::a($model->event->name, ['view', 'id' => $model->id]);
                },
            ],
            'seats',
            'freeSeats',
            [
                'attribute' => 'sex',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model common\models\Ride */
                    return \common\models\Ride::getSexes()[$model->sex];
                },
            ],
            'date_start:date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{record} {update}',
                'buttons' => [
                    'record' => function ($url, $model, $key) {
                        return $model->canRecord ? Html::a('Записать', ['record', 'id' => $model->id], ['title' => 'Записать', 'class' => 'btn btn-xs btn-success']) : '';
                    },
                    'update' => function ($url, $model, $key) {
                        return $model->isFuture ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => 'Редактировать']) : Html::a('Клонировать', ['clone', 'id' => $model->id], ['title' => 'Клонировать', 'class' => 'btn btn-xs btn-primary']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
