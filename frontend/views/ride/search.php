<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск поездок';
$this->params['breadcrumbs'][] = ['label' => 'Мои поездки', 'url' => ['my-rides']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-index">

    <p>Не смогли найти нужную поездку? <?= Html::a('Создайте запрос на доставку!', ['request/create'], ['class' => 'btn btn-warning']) ?></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'rowOptions' => function ($model) {
            return $model->freeSeats > 0 ? ['class' => 'success'] : ['class' => 'danger'];
        },
        'columns' => [
            'event.name',
            'seats',
            'freeSeats',
            'date_start:date',
            [
                'attribute' => 'user.image',
                'format' => 'raw',
                'label' => 'Водитель',
                'value' => function ($model) {
                    /** @var $model common\models\Ride */
                    return '<a target="_blank" href="' . $model->user->getImageUrl() . '"><span class="glyphicon glyphicon-camera"></span></a>&nbsp;' . $model->user->name;
                },
            ],
            [
                'attribute' => 'user.sex',
                'format' => 'raw',
                'label' => 'Пол водителя',
                'value' => function ($model) {
                    /** @var $model common\models\Ride */
                    return \common\models\Ride::getSexes()[$model->user->sex];
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{checkin} {view}',
                'buttons' => [
                    'checkin' => function ($url, $model, $key) {
                        return $model->canCheckIn ? Html::a('Записаться', ['check-in', 'id' => $model->id], ['title' => 'Записаться', 'class' => 'btn btn-xs btn-success']) : '';
                    },
                ],
            ],
        ],
    ]); ?>
</div>
