<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CheckInForm */

$this->title = 'Запись на поездку';
$this->params['breadcrumbs'][] = ['label' => 'Поиск поездок', 'url' => ['search']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-create">

    <div class="ride-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'seats')->textInput()->hint('На данный момент доступно мест в машине: ' . $model->model->freeSeats) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
