<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ride */

$this->title = 'Update Ride: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Rides', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ride-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
