<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ride */

$this->title = 'Создание поездки';
$this->params['breadcrumbs'][] = ['label' => 'Все поездки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
