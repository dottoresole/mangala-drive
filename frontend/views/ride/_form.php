<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Ride */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ride-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'event_id')->dropDownList(ArrayHelper::map(\common\models\Event::getAllRecords(), 'id', 'name'), ['prompt' => 'Выберите..']) ?>

    <?= $form->field($model, 'districtsArray')->checkboxList(ArrayHelper::map(\common\models\District::getAllRecords(), 'id', 'name'))->label('Проезжаю районы') ?>

    <?= $form->field($model, 'seats')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_start')->widget(\yii\jui\DatePicker::className(), [
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
        'clientOptions' => [
            'minDate' => new yii\web\JsExpression("new Date()"),
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
