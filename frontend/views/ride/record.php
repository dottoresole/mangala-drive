<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Record */

$this->title = 'Записать на поездку';
$this->params['breadcrumbs'][] = ['label' => 'Список пассажиров', 'url' => ['view', 'id' => $model->model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ride-create">
    <div class="ride-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'phone') ?>

        <?= $form->field($model, 'seats')->textInput()->hint('На данный момент доступно мест в машине: ' . $model->model->freeSeats) ?>

        <div class="form-group">
            <?= Html::submitButton('Записать на поездку', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
