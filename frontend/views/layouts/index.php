<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;
use frontend\widgets\Menu;
$this->beginContent('@frontend/views/layouts/base.php');
?>
<body class="home">
    <?php $this->beginBody() ?>
    <div class="man"></div>
    <div id="wrapper">
      <div id="header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="col-xs-4 custom-col-1">
                <strong class="logo logo-1"><a href="<?= Url::to(['main/index']) ?>">Гаура-клуб</a></strong>
                <h2 class="slogan">Клуб Харидаса Тхакура</h2>
                <a href="<?= Url::to(['main/article-view', 'slug' => 'o-klube']) ?>" class="btn btn-default btn-orange">Узнать больше</a>
              </div>
              <div class="col-xs-4 col-xs-offset-4  custom-col-2">
                <strong class="logo logo-2"><a href="<?= Url::to(['main/index']) ?>">Мангала Драйв</a></strong>
              </div>
            </div>
          </div>
        </div>
        <?= Menu::widget(); ?>
      </div>
      <div id="main">
        <div class="container">
          <?= Alert::widget() ?>
          <?= $content ?>
      </div>
    </div>
<?php $this->endContent(); ?>
