<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\widgets\Alert;
$this->beginContent('@frontend/views/layouts/base.php');
?>
<body class="inner">
    <?php $this->beginBody() ?>
    <div id="wrapper">
      <div id="header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                <div class="nav-m">
                        <strong class="logo logo-1"><a href="<?= Url::to(['site/index']) ?>">Гаура-клуб</a></strong>
                        <strong class="logo logo-2"><a href="<?= Url::to(['site/index']) ?>">Мангала Драйв</a></strong>
                        <div class="drop-prof dropdown">
                            <?if(\Yii::$app->user->isGuest):?>
                                <a href="javascript:void(0);" class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" data-hover="dropdown">
                                    <em class="ava"><img src="/images/ava.png" alt=""></em>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?= Url::to(['site/login']) ?>">Войти</a></li>
                                    <li><a href="<?= Url::to(['site/signup']) ?>">Зарегистрироваться</a></li>
                                </ul>
                            <?else:?>
                                <a href="javascript:void(0);" class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" data-toggle="dropdown" data-hover="dropdown">
                                    <em class="ava"><img width="60" src="<?= \Yii::$app->user->identity->imageUrl ?>" alt=""></em>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?= Url::to(['site/profile']) ?>">Ваши настройки</a></li>
                                    <li><a href="<?= Url::to(['site/logout']) ?>" data-method="post" target="_self">Выход</a></li>
                                </ul>
                            <?endif;?>
                        </div>
                    <?
                    $menuItems = [];
                    if (Yii::$app->user->isGuest) {
                        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
                        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
                    } else {
                        $menuItems[] = ['label' => 'Для водителя', 'url' => ['/ride/index']];
                        $menuItems[] = ['label' => 'Для пассажира', 'url' => ['/ride/my-rides']];
                    }
                    NavBar::begin([
                        'containerOptions' => [
                            'id' => 'navbar',
                        ],
                        'options' => [
                            'class' => 'navbar navbar-inverse',
                        ],
                    ]);
                    echo Nav::widget([
                        'options' => [
                            'class' => 'nav navbar-nav',
                        ],
                        'items' => $menuItems
                    ]);
                    NavBar::end();
                    ?>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div id="main">
        <div class="container">
            <!-- breadcrumbs -->
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <!-- breadcrumbs end -->
            <?if(empty($this->params['innerTitle'])):?>
            <h1><?= Html::encode($this->title) ?></h1>
            <?endif;?>
            <?= Alert::widget() ?>
            <?= $content ?>
      </div>
    </div>
<?php $this->endContent(); ?>
