<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> &ndash; Mangala Drive</title>
    <?php $this->head() ?>
</head>
      <?= $content ?>
      <div id="footer">
        <div class="container">
          <div class="col-md-12">
            <span class="copy col-md-6">Mangaladrive &copy; <?= date('Y') ?></span>
            <span class="made col-md-6 text-right">Сайт разработан и поддерживается участниками <a href="http://www.gauraclub.ru/">Гаура Клуб</a></span>
          </div>
        </div>
      </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
