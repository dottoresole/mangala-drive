<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserRideAssignment */

$this->title = 'Create User Ride Assignment';
$this->params['breadcrumbs'][] = ['label' => 'User Ride Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-ride-assignment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
