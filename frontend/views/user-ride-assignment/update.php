<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserRideAssignment */

$this->title = 'Update User Ride Assignment: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'User Ride Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-ride-assignment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
