<?php

namespace frontend\models;

use common\models\Ride;
use common\models\UserRideAssignment;
use Yii;
use yii\base\Model;

/**
 * Class Record
 * @package app\models
 */
class Record extends Model
{
    public $seats;

    public $name;

    public $phone;

    private $model;

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['seats', 'name', 'phone'], 'required'],
            ['seats', 'integer'],
            ['seats', 'validateSeats'],
            [['name', 'phone'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'seats' => 'Зарезервировать места',
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @param $attribute
     */
    public function validateSeats($attribute)
    {
        if ($this->seats > $this->model->freeSeats) {
            $this->addError($attribute, 'Недоступно такое количество мест.');
            return;
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $model = new UserRideAssignment([
                'name' => $this->name,
                'phone' => $this->phone,
                'ride_id' => $this->model->id,
                'seats_reserved' => $this->seats,
            ]);

            return $model->save();
        }

        return false;
    }

    /**
     * @return Ride
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Ride $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}
