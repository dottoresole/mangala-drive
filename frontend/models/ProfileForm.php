<?php

namespace frontend\models;

use common\models\traits\UploadTrait;
use common\models\User;
use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

/**
 * Profile form
 */
class ProfileForm extends Model
{
    use UploadTrait;

    const SCENARIO_WITH_IMAGE = 'create';

    public $username;
    public $email;
    public $phone;
    public $name;
    public $image;
    public $region;
    public $sex;

    /**
     * @var \common\models\User
     */
    private $_user;

    public function init()
    {
        $this->_user = Yii::$app->user->identity;
        $this->setAttributes([
            'username' => $this->_user->username,
            'name' => $this->_user->name,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'sex' => $this->_user->sex,
            'image' => $this->_user->image,
            'region' => $this->_user->region,
        ]);

        return parent::init();
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_WITH_IMAGE] = $scenarios['default'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sex', 'phone'], 'required'],
            ['sex', 'in', 'range' => array_keys(User::getSexes())],
            [['name', 'phone'], 'string', 'min' => 2, 'max' => 255],
            [['username', 'region', 'image', 'email'], 'safe'],
        ];
    }

    public function updateProfile()
    {
        $this->_user->name = $this->name;
        $this->_user->phone = $this->phone;

        return $this->_user->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'email' => 'E-mail',
            'name' => 'Ваше имя',
            'phone' => 'Телефон',
            'image' => 'Фотография',
            'sex' => 'Sex',
        ];
    }
}
