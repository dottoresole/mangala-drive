<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Settings form
 */
class ChangePasswordForm extends Model
{
    public $passwordOld;
    public $password;
    public $passwordRepetition;

    /**
     * @var \common\models\User
     */
    private $_user;

    public function init()
    {
        $this->_user = Yii::$app->user->identity;

        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'passwordRepetition', 'passwordOld'], 'required'],
            ['password', 'string', 'min' => 6],
			['passwordRepetition', 'compare', 'compareAttribute' => 'password'],
			['passwordOld', 'validateCurrentPassword'],
        ];
    }

	public function validateCurrentPassword()
	{
        $user = $this->_user;
		if (!$user->validatePassword($this->passwordOld)) {
			$this->addError('passwordOld', 'Вы неправильно ввели текущий пароль.');
		}
	}

    public function updatePassword()
    {
        if ($this->validate()) {
            $this->_user->setPassword($this->password);
            $this->_user->generateAuthKey();

            return $this->_user->save();
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passwordOld' => 'Текущий пароль',
            'password' => 'Новый пароль',
            'passwordRepetition' => 'Повторите новый пароль',
        ];
    }
}
