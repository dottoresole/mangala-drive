<?php
namespace frontend\models;

use common\models\traits\UploadTrait;
use yii\base\Model;
use common\models\User;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{
    use UploadTrait;

    public $acceptRules;
    public $username;
    public $email;
    public $name;
    public $phone;
    public $sex;
    public $country;
    public $city;
    public $district;
    public $password;
    public $imageFile;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Пользователь с таким e-mail уже существует.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['username', 'safe'],

            [['city', 'country', 'district'], 'required'],
            [['city', 'country', 'district'], 'integer'],

            [['name', 'phone', 'sex'], 'required'],

            ['imageFile', 'image', 'skipOnEmpty' => false],

            ['acceptRules', 'required', 'requiredValue' => 1, 'message' => 'Вы должны согласиться с правилами системы'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Введите ваш e-mail',
            'password' => 'Придумайте пароль',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'sex' => 'Пол',
            'country' => 'Страна',
            'city' => 'Город',
            'district' => 'Район проживания',
            'imageFile' => 'Фотография',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            print_r($this->errors);exit;
            return null;
        }

        $user = new User();
        $user->username = \Yii::$app->security->generateRandomString(16);
        $user->email = $this->email;
        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->sex = $this->sex;
        $user->country_id = $this->country;
        $user->city_id = $this->city;
        $user->district_id = $this->district;
        $user->status = User::STATUS_PENDING;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($image = $this->uploadImageFile()) {
            $user->image = $image;
        }

        return $user->save() ? $user : null;
    }
}
