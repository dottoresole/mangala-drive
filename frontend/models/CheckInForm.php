<?php

namespace frontend\models;

use common\models\Ride;
use common\models\UserRideAssignment;
use Yii;
use yii\base\Model;

/**
 * Class CheckInForm
 * @package app\models
 */
class CheckInForm extends Model
{
    public $seats;

    private $model;

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['seats'], 'required'],
            ['seats', 'integer'],
            ['seats', 'validateSeats'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'seats' => 'Зарезервировать места',
        ];
    }

    /**
     * @param $attribute
     */
    public function validateSeats($attribute)
    {
        if ($this->seats > $this->model->freeSeats) {
            $this->addError($attribute, 'Недоступно такое количество мест.');
            return;
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $model = new UserRideAssignment([
                'user_id' => Yii::$app->user->id,
                'ride_id' => $this->model->id,
                'seats_reserved' => $this->seats,
            ]);

            return $model->save();
        }

        return false;
    }

    /**
     * @return Ride
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Ride $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}
