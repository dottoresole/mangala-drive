<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#panel1">Активные
                <span class="badge"><?= $activeUserProvider->getTotalCount() ?></span>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#panel2">Ожидающие модерации
                <span class="badge"><?= $pendingUserProvider->getTotalCount() ?></span>
            </a>
        </li>
    </ul>
    <br /><br />
    <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <?= GridView::widget([
                'dataProvider' => $activeUserProvider,
                'filterModel' => $searchActiveUser,
                'columns' => [
                    'id',
                    'name',
                    'phone',
                    'email:email',
                    'sexName',
                    'region',
                    [
                        'attribute' => 'imageUrl',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {
                            return '<a target="_blank" href="' . $model->imageUrl . '"><span class="glyphicon glyphicon-picture"></span></a>';
                        },
                    ],
                    'created_at:datetime',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
        <div id="panel2" class="tab-pane fade in">
            <?= GridView::widget([
                'dataProvider' => $pendingUserProvider,
                'filterModel' => $searchPendingUser,
                'columns' => [
                    'id',
                    'name',
                    'phone',
                    'email:email',
                    'sexName',
                    'region',
                    [
                        'attribute' => 'imageUrl',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {
                            return '<a target="_blank" href="' . $model->imageUrl . '"><span class="glyphicon glyphicon-picture"></span></a>';
                        },
                    ],
                    'created_at:datetime',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{accept} {update} {delete}',
                        'buttons' => [
                            'accept' => function ($url, $model, $key) {
                                return Html::a('<span title="Одобрить заявку" class="glyphicon glyphicon-ok"></span>', $url);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
