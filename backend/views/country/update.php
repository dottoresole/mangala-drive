<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$this->title = 'Страна: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все страны', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="country-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
