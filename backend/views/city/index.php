<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $country->name . ': список городов';
$this->params['breadcrumbs'][] = ['label' => 'Все страны', 'url' => ['/country/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать город', ['create', 'country_id' => $country->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function ($model, $key, $index, $column) {
                    return '<a href="' . \yii\helpers\Url::to(['district/index', 'city_id' => $model->id]) . '">' . $model->name . '</a>';
                },
            ],
            [
                'attribute' => 'country.name',
                'label' => 'Страна',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
