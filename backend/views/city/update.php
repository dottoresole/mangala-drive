<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = 'Город: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все города', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
