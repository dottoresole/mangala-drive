function initNav()
{
	var nav = document.getElementById("ava-list");
	if(nav)
	{
		var lis = nav.getElementsByTagName("span");
		for (var i = 0; i < lis.length; i++)
		{
			lis[i].style.zIndex = 100 - i;
		}
	}
}

function addSpinnerBefore(id){
    //$(id).before("<div class='loading'><img src='/images/loading.gif' /></div>");
    $(id).fadeTo('fast', 0);
}

function removeSpinner(id){
    $(".loading").remove();
    $(id).fadeTo('slow', 1);
}

function yatraContact() {
	if ( $('.yatraContact').length ) {
	    $('.yatraContact').change(function() {
	        $id = $(this).val();
    	    addSpinnerBefore('#contact .col-md-12');
            $.ajax({
                'method': "POST",
                'dataType': "json",
                'url': '/main/yatra-info-ajax',
                'data': { 'id': $id }
            })
            .done(function(arrayData) {
                data = arrayData['model'];
                $("#yatraCity").html(data.city);
                $("#yatraAddress").html(data.address);
                $("#yatraEmail").html('<a href="mailto:'+data.email+'">'+data.email+'</a>');
                $("#yatraPhone").html(data.phone);
                $("#yatraDescription").html(data.description_short);
                $("#map").html('');
                $.getScript(arrayData['mapLink'] + "&id=map&rnd=" + Math.random());
        	    removeSpinner('#contact .col-md-12');
            });
	    });
	}
}

if (window.addEventListener)
	window.addEventListener("load", initNav, false);
else if (window.attachEvent)
	window.attachEvent("onload", initNav);

$(function() {
    $('.post-blog, .big-post').matchHeight();
    yatraContact();
    $(".toggle-comment-form").click(function(){
        $(".ask-form").toggle();
    });
    $(".toggle-statistic").click(function(){
        $(".statistic-data").toggle();
    });
    $(".checkin-button-lg").click(function(){
	        $obj = $(this);
            $cntObj = $(".stat-count");
            $cnt = parseInt($cntObj.html());
            $.ajax({
                'method': "GET",
                'dataType': "json",
                'url': '/dashboard/check-challenge',
                'data': { 'id': $obj.attr('data-id') }
            })
            .done(function(data) {
                if (data > 0) {
                    $obj.addClass('active');
                    $cntObj.html($cnt + 1);
                    alert('Отметка о сегодняшнем достижении цели успешно сохранена!');
                } else {
                    $obj.removeClass('active');
                    if ($cnt > 0) {
                        $cntObj.html($cnt - 1);
                    }
                    alert('Отметка удалена.');
                }
            });
    });
});
